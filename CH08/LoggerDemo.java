import java.util.logging.*;

public class LoggerDemo {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger(LoggerDemo.class.getName());
        logger.log(Level.WARNING, "WARNING ѶϢ");
        logger.log(Level.INFO, "INFO ѶϢ");
        logger.log(Level.CONFIG, "CONFIG ѶϢ");
        logger.log(Level.FINE, "FINE ѶϢ");
    }
}
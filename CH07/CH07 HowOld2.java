import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.Scanner;

import static java.lang.System.out;

public class HowOld2 {
    public static void main(String[] args) {
        out.print("请输入出生年月日（yyyy-mm-dd）：");
        LocalDate birth = LocalDate.parse(new Scanner(System.in).nextLine());
        Date now1 = new Date();
        LocalDate now = LocalDate.now();
        Period period = Period.between(birth, now);
        out.printf("你活了 %d 年 %d 月 %d 日%n", period.getYears(), period.getMonths(), period.getDays());
        Date now2 = new Date();
        long time = now2.getTime() - now1.getTime();
        out.printf("运行了：%d 毫秒%n", (time));
    }
}
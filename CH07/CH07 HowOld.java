import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class HowOld {
    public static void main(String[] args) throws Exception {
        System.out.print("输入出生年月日(yyyy-mm-dd):");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        Date begin = new Date();
        Date birthday = dateFormat.parse(new Scanner(System.in).nextLine());
        Date currentDate = new Date();
        long life = currentDate.getTime() - birthday.getTime();
        System.out.println("你今年的岁数为：" + (life / (365 * 24 * 60 * 60 * 1000L)));
        Date end = new Date();
        long duration = end.getTime() - begin.getTime();
        System.out.println("运行时间：" + (duration));
    }
}
import java.util.Calendar;
import java.util.TimeZone;

import static java.lang.System.out;

public class TimeZoneDemo2 {
    public static void main(String[] args) {
        TimeZone beijingTz = TimeZone.getTimeZone("Asia/Shanghai");
        Calendar calendar = Calendar.getInstance(beijingTz);
        showTime(calendar);
        TimeZone copenhangeTz = TimeZone.getTimeZone("Europe/Copenhagen");
        calendar.setTimeZone(copenhangeTz);
        showTime(calendar);
    }

    static void showTime(Calendar calendar) {
        out.print(calendar.getTimeZone().getDisplayName());
        out.printf(" %d:%d%n", calendar.get(Calendar.HOUR),
                calendar.get(Calendar.MILLISECOND));
    }
}
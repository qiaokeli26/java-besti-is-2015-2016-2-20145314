public class DateTest {
    public static void main(String args[]) {
        DateTest nowDate = new DateTest();
        nowDate.getSystemCurrentTime();
        nowDate.getCurrentDate();
    }

    public void getSystemCurrentTime() {
        System.out.println("----获取系统当前时间----");
        System.out.println("系统当前时间 = " + System.currentTimeMillis());
    }

    public void getCurrentDate() {
        System.out.println("----获取系统当前日期----");
        //创建并初始化一个日期（初始值为当前日期）
        Date date = new Date();
        System.out.println("现在的日期是 = " + date.toString());
        System.out.println("自1970年1月1日0时0分0秒开始至今所经历的毫秒数 = " + date.getTime());
    }
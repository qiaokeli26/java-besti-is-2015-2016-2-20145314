import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static java.lang.System.out;
import static java.util.Comparator.comparing;

class Customer {
    private String firstName;
    private String lastName;
    private Integer zipCode;

    public Customer(String firstName, String lastName, Integer zipCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.zipCode = zipCode;
    }

    public String toString() {
        return String.format("Customer(%s %s, %d)", firstName, lastName, zipCode);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getZipCode() {
        return zipCode;
    }
}

public class CustomerDemo {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
                new Customer("Justin", "Lin", 804),
                new Customer("Monica", "Huang", 804),
                new Customer("Irene", "Lin", 804),
                new Customer("Justin", "Xin", 804),
                new Customer("Yeeeee", "Lin", 604),
                new Customer("Justin", "Zhang", 604)
        );
        Comparator<Customer> byLastName = comparing(Customer::getLastName);
        customers.sort(
                byLastName
                        .thenComparing(Customer::getFirstName)
                        .thenComparing(Customer::getZipCode)
        );
        customers.forEach(out::println);
    }
}